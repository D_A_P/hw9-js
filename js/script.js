'use strict';

// task 1
const elementFeature1 = document.getElementsByClassName('feature');
for(let i = 0; i < elementFeature1.length; i++) {
    elementFeature1[i].style.textAlign = "center";
};
console.log('method-1:', elementFeature1);
 
const elementFeature2 = document.querySelectorAll('.feature');
elementFeature2.forEach(element => {
    element.style.textAlign = "center";
})
console.log('method-2:', elementFeature2);

// task 2
const h2Element = document.getElementsByTagName('h2');
for (let i = 0; i < h2Element.length; i++){
    h2Element[i].textContent = "Awesome feature";
}
console.log(h2Element);

// task 3
const elementFeatureTitle = document.getElementsByClassName('feature-title');
for( let i = 0; i < elementFeatureTitle.length; i++){
    elementFeatureTitle[i].textContent += '!';    
};
console.log(elementFeatureTitle);